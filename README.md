# GitLab API

Python package for gitlab api v4


**Installation**

```
$ pip install gitlabapi
```


**Using**

**- Import**

```
from gitlabapi import GitLab4
```

**- Create GitLab4 object**

```
api_url = 'https://gitlab.com/api/v4'
token = 'some_token'
api = GitLab4(api_url, personal_access_token=token)
```
