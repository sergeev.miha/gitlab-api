import os
import unittest

from gitlabapi import GitLab4

try:
    PERSONAL_ACCESS_TOKEN = os.environ['GITLABAPI_PERSONAL_ACCESS_TOKEN']
except Exception:
    message = 'Variable GITLABAPI_PERSONAL_ACCESS_TOKEN not set'
    exit(message)

CI_PROJECT_ID = os.environ.get('CI_PROJECT_ID', 18190451)

API_URL = 'https://gitlab.com/api/v4'


class Test_GitLab4(unittest.TestCase):

    def setUp(self):
        self.api = GitLab4(API_URL,
                           personal_access_token=PERSONAL_ACCESS_TOKEN)

    def test_version(self):
        version = self.api.version()
        version['version']
        version['revision']

    def test_list_repository_tree(self):
        tree = self.api.list_repository_tree(CI_PROJECT_ID)
        self.assertNotEqual(len(tree), 0)

    def test_list_repository_tree_not_existing_path(self):
        tree = self.api.list_repository_tree(CI_PROJECT_ID, path='not exist')
        self.assertEqual(len(tree), 0)

    def test_list_repository_tree_develop_ref(self):
        tree = self.api.list_repository_tree(CI_PROJECT_ID, ref='develop')
        self.assertNotEqual(len(tree), 0)

    def test_list_repository_tree_develop_ref_recursive(self):
        tree = self.api.list_repository_tree(CI_PROJECT_ID, ref='develop')
        recursive_tree = self.api.list_repository_tree(
            CI_PROJECT_ID, ref='develop', recursive=True
        )
        self.assertTrue(len(recursive_tree) > len(tree))


if __name__ == "__main__":
    unittest.main()
