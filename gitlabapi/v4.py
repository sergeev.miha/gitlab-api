from functools import wraps
from typing import Any, Callable

from requests import Session
from requests.utils import requote_uri


def _get_all_pages(function: Callable):
    @wraps(function)
    def wrapper(self, *args, **kwargs):
        headers = {
            'X-Page': '1',
            'X-Per-Page': str(self._max_items_per_page)
        }
        self._session.headers.update(headers)
        result = []
        while True:
            response = function(self, *args, **kwargs)
            response.raise_for_status()
            result.extend(response.json())
            try:
                x_next_page = response.headers['X-Next-Page']
            except Exception:
                break
            if x_next_page:
                self._session.headers.update(
                    {'X-Page': response.headers['X-Next-Page']}
                )
            else:
                break
        return result
    return wrapper


class GitLab4:

    def __init__(self, api_url: str, personal_access_token: str):
        '''https://docs.gitlab.com/ee/api/README.html#personal-access-tokens'''

        self._api_url = api_url

        headers = {'Private-Token': personal_access_token}
        self._session = Session()
        self._session.headers.update(headers)

        self._max_items_per_page = 100

    def __del__(self):
        self._session.close()

    def _get(self, url: str):
        request_url = requote_uri(f'{self._api_url}{url}')
        response = self._session.get(request_url)
        return response

    def _post(self, url: str, data: Any):
        request_url = requote_uri(f'{self._api_url}{url}')
        response = self._session.post(request_url, data=data)
        return response

    def _query_string_params(self, **kwargs):
        result = ''
        for key, value in kwargs.items():
            if value:
                result += f'{"&" if "?" in result else "?"}{key}={value}'
        return result

    '''Repositories API'''

    @_get_all_pages
    def list_repository_tree(self, id: int, path: str = None, ref: str = None,
                             recursive: bool = None):
        '''https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree'''

        url = f'/projects/{id}/repository/tree'
        url += self._query_string_params(path=path, ref=ref,
                                         recursive=recursive)
        return self._get(url)

    '''Version API'''

    def version(self):
        '''https://docs.gitlab.com/ee/api/version.html'''

        url = '/version'
        response = self._get(url)
        return response.json()
