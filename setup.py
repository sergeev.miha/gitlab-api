from setuptools import find_packages, setup

import gitlabapi

with open('README.md', 'r', encoding='utf-8') as f:
    long_description = f.read()

with open('requirements.txt', 'r', encoding='utf-8') as f:
    requires = [s.strip() for s in f.readlines()]

__url__ = 'https://gitlab.com/sergeev.miha/gitlab-api'

setup(
    name='gitlabapi',
    version=gitlabapi.__version__,
    packages=find_packages(exclude=[
        '*vscode*',
        '*venv*',
        '*tests*',
        '*egg-info*'
    ]),
    long_description=long_description,
    install_requires=requires,
    url=__url__,
    test_suite='tests'
)
